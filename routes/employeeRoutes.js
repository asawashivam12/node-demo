var express = require("express");
var router = express.Router();
const { verifyToken } = require("../utils/authorization");
const {
  getEmployeeRoles,
  AddEmployee,
  getEmployeeList,
  uploadFile,
  saveEmployeeProfile,
} = require("../controllers/employeeController");

router.get("/GetEmployeeRole", verifyToken, getEmployeeRoles);
router.post("/PostEmployee", verifyToken, AddEmployee);
router.post("/GetEmployeeList", verifyToken, getEmployeeList);
router.post("/UploadFile", verifyToken, uploadFile);
router.post("/SaveEmployeeProfile", verifyToken, saveEmployeeProfile);

module.exports = router;
