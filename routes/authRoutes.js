var express = require("express");
var router = express.Router();
const { validateEmployee } = require("../controllers/authController");

router.post("/Login", validateEmployee);

module.exports = router;
