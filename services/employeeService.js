const db = require("../dbconnection");

module.exports = {
  getEmployeeRoles: () => {
    return new Promise((resolve, reject) => {
      db.query(`select * from roles`, [], (error, result, fields) => {
        if (error) {
          return reject(error);
        }
        return resolve(result);
      });
    });
  },

  checkDuplicateEmail: (email) => {
    return new Promise((resolve, reject) => {
      db.query(
        `select * from employees where email = ?`,
        [email],
        (error, result, fields) => {
          if (error) {
            return reject(error);
          }
          return resolve(result);
        }
      );
    });
  },

  AddEmployee: (data) => {
    const date = new Date();
    const iOperation = Number(data.iOperation);
    if (iOperation === 0) {
      return new Promise((resolve, reject) => {
        db.query(
          `insert into employees(firstName,lastName,email,password,contactNo,empRoleID,empReferenceID,dob,city,address,pincode,empDesignation,iStatus,gender,createdDate,updateDate) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [
            data.firstName,
            data.lastName,
            data.email,
            data.password,
            data.contactNo,
            data.empRoleID,
            data.empReferenceID,
            data.dob,
            data.city,
            data.address,
            data.pincode,
            data.empDesignation,
            1,
            data.gender,
            date,
            date,
          ],
          (error, result, fields) => {
            if (error) {
              return reject(error);
            }
            return resolve(result);
          }
        );
      });
    } else if (iOperation === 1) {
      return new Promise((resolve, reject) => {
        db.query(
          "UPDATE employees SET `firstName`=?,`lastName` = ?,`contactNo`=?,`empRoleID`=?,`empReferenceID`=?,`dob`=?,`city`=?,`address`=?,`pincode`=?,`empDesignation`=?,`gender`=?,`updateDate`=? WHERE `_id`= ?",
          [
            data.firstName,
            data.lastName,
            data.contactNo,
            data.empRoleID,
            data.empReferenceID,
            data.dob,
            data.city,
            data.address,
            data.pincode,
            data.empDesignation,
            data.gender,
            date,
            data._id,
          ],
          (error, result, fields) => {
            if (error) {
              return reject(error);
            }
            return resolve(result);
          }
        );
      });
    } else if (iOperation === 2) {
      return new Promise((resolve, reject) => {
        db.query(
          "UPDATE employees SET `iStatus`= ? where `_id` =?",
          [0, data._id],
          (error, result, fields) => {
            if (error) {
              return reject(error);
            }
            return resolve(result);
          }
        );
      });
    }
  },

  getEmployeeList: (data) => {
    return new Promise((resolve, reject) => {
      db.query(
        `select * from employees where iStatus = ?`,
        [data.iStatus],
        (error, result, fields) => {
          if (error) {
            return reject(error);
          }
          return resolve(result);
        }
      );
    });
  },
  saveEmployeeProfile: (data) => {
    return new Promise((resolve, reject) => {
      db.query(
        `UPDATE  employees SET firstName= ?, lastName = ?,email=?, password = ?, contactNo=?, city = ?, profileUrl=? where _id = ?`,
        [
          data.firstName,
          data.lastName,
          data.email,
          data.password,
          data.contactNo,
          data.city,
          data.profileUrl,
          data._id,
        ],
        (error, result, fields) => {
          if (error) {
            return reject(error);
          }
          return resolve(result);
        }
      );
    });
  },
};
