const db = require("../dbconnection");

module.exports = {
  validateEmployee: (email) => {
    return new Promise((resolve, reject) => {
      db.query(
        `select * from employees where email = ?`,
        [email],
        (error, result, fields) => {
          if (error) {
            return reject(error);
          }
          return resolve(result[0]);
        }
      );
    });
  },
};
