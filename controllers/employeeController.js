const AppError = require("../utils/appError");
const catchAsync = require("../utils/catchAsync");
var { genSaltSync, hashSync } = require("bcrypt");
const mailer = require("nodemailer");
const path = require("path");
const util = require("util");

const {
  getEmployeeRoles,
  checkDuplicateEmail,
  AddEmployee,
  getEmployeeList,
  saveEmployeeProfile,
} = require("../services/employeeService");

module.exports = {
  getEmployeeRoles: catchAsync(async (req, res, next) => {
    const result = await getEmployeeRoles();
    if (!result.length) throw new AppError("Records Not Found", 404);
    return res.status(200).json({
      success: 1,
      data: result,
    });
  }),

  AddEmployee: catchAsync(async (req, res, next) => {
    const body = req.body;
    const nonEncryptedPasswrod = req.body.password;
    const employeeID = +req.body._id;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const contactNo = req.body.contactNo;
    const password = req.body.password;
    const roleID = req.body.empRoleID;
    const iOperation = req.body.iOperation;
    // iOperation Flag Is Required
    if (iOperation === "" || iOperation === undefined)
      throw new AppError("Please Add Operational Flag", 401);

    if (iOperation === 0) {
      // Add Operation
      const emailExists = await checkDuplicateEmail(body.email);
      const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
      const validateEmail = email.match(emailRegexp);
      const strMobileNo = JSON.stringify(contactNo);
      var mobileRegexp = /^\d{10}$/;
      const validMobeNo = strMobileNo.match(mobileRegexp);
      if (emailExists.length !== 0)
        throw new AppError(`${body.email} Already Exists`, 409);
      if (!validateEmail) throw new AppError("Invalid Email Format !!", 422);
      if (!firstName)
        throw new AppError("First Name Is Mandatory Required", 401);
      if (!lastName) throw new AppError("Last Name Is Mandatory Required", 422);
      if (!email) throw new AppError("Email Is Mandatory Required", 422);
      if (!password) throw new AppError("Password Is Mandatory Required", 422);
      if (!validMobeNo) throw new AppError("Invalid Mobile Number", 422);
      if (roleID === "" || roleID === undefined)
        throw new AppError("Role is Mandatory", 422);
    } else if (iOperation === 1) {
      // Update Operation
      const strMobileNo = JSON.stringify(contactNo);
      var regExp = /^\d{10}$/;
      const validMobeNo = strMobileNo.match(regExp);
      if (!employeeID) throw new AppError("Employee ID Required", 422);
      if (!firstName)
        throw new AppError("First Name Is Mandatory Required", 422);
      if (!lastName) throw new AppError("Last Name Is Mandatory Required", 422);
      if (!validMobeNo) throw new AppError("Invalid Mobile Number", 422);
      if (roleID === "" || roleID === undefined)
        throw new AppError("Employee Role is Mandatory", 422);
    } else if (iOperation === 2) {
      //  Delete Operation
      if (!employeeID) throw new AppError("Employee ID Required", 422);
    }
    let resMessage;
    if (iOperation === 0) {
      const salt = genSaltSync(10);
      body.password = hashSync(body.password, salt);
      resMessage = "Registered";
    } else if (iOperation === 1) {
      resMessage = "Updated";
    } else if (iOperation === 2) {
      resMessage = "Deleted";
    }
    const result = await AddEmployee(body);
    if (!result) throw new AppError("Records Not Found", 404);
    // write code For Email Over Here
    if (iOperation === 0) {
      const transporter = mailer.createTransport({
        service: "gmail",
        auth: {
          user: process.env.EMAIL,
          pass: process.env.PASSWORD,
        },
      });

      let emailbody = {
        from: process.env.EMAIL,
        to: req.body.email,
        subject: "Welcome To Intelliatech Portal",
        attachments: [
          {
            filename: "image.png",
            path: "./public/images/intellia_logo.png",
            cid: "intellia_logo", //same cid value as in the html img src
          },
        ],
        html: `<p>Hi <b>${req.body.firstName.toUpperCase()} ${req.body.lastName.toUpperCase()}</b></p>
              <p>Your Email And Password Is As Below</p>
              <p>Email =  ${req.body.email}</p>
              <p>Password =  ${nonEncryptedPasswrod}</p>
              <p>Please Use This Email And Password To Login Your Account </p>
              <img width="350px" height="100" src="cid:intellia_logo">`,
      };
      transporter.sendMail(emailbody, (err, result) => {
        if (err) {
          console.log(err);
          return false;
        }
      });
    }

    return res.status(200).json({
      success: 1,
      message: `Employee ${resMessage} Successfully`,
    });
  }),

  getEmployeeList: catchAsync(async (req, res, next) => {
    const body = req.body;
    const result = await getEmployeeList(body);
    if (!result.length) throw new AppError("Records Not Found", 404);
    return res.status(200).json({
      success: 1,
      data: result,
    });
  }),

  uploadFile: catchAsync(async (req, res, next) => {
    const file = req.files.upfile;
    const fileName = file.name;
    const extension = path.extname(fileName);
    const allowedExtension = /png|jpeg|jpg|gif/;
    if (!allowedExtension.test(extension))
      throw new AppError("Unsupported Media Extension", 422);
    const md5 = file.md5;
    Url = "/uploads/" + md5 + extension;
    await util.promisify(file.mv)("./public" + Url);
    res.json({
      success: 1,
      message: "File Uploaded Successfully",
      Url: `http://${req.headers.host}${Url}`,
    });
  }),

  saveEmployeeProfile: catchAsync(async (req, res, next) => {
    console.log(req.body);
    const body = req.body;
    const employeeID = body._id;
    const firstName = body.firstName;
    const lastName = body.lastName;
    const email = body.email;
    const password = body.password;
    const contactNo = body.contactNo;

    if (!employeeID)
      throw new AppError("Employee Id Is Mandatory Or Required", 422);
    if (!firstName)
      throw new AppError("First Name Is Mandatory Or Required", 422);
    if (!lastName)
      throw new AppError("Last Name Is Mandatory Or Required", 422);
    if (!email) throw new AppError("Email Is Mandatory Or Required", 422);
    if (!password) throw new AppError("Password Is Mandatory Or Required", 422);
    if (!contactNo)
      throw new AppError("Contact No Is Mandatory Or Required", 422);
    const salt = genSaltSync(10);
    body.password = hashSync(body.password, salt);
    const result = await saveEmployeeProfile(body);
    if (!result) throw new AppError("Records Not Found", 404);
    return res.status(200).json({
      success: 1,
      message: "Profile Updated Successfully",
    });
  }),
};
