const AppError = require("../utils/appError");
const catchAsync = require("../utils/catchAsync");
const { sign } = require("jsonwebtoken");
var { genSaltSync, hashSync, compareSync } = require("bcrypt");
const { validateEmployee } = require("../services/authService");

module.exports = {
  validateEmployee: catchAsync(async (req, res, next) => {
    const body = req.body;
    const email = req.body.email;
    const password = req.body.password;

    if (!email) throw new AppError("Email Field Is Mandatory Or Required", 401);
    if (!password)
      throw new AppError("Password Field Is Mandatory Or Required", 401);
    const result = await validateEmployee(body.email);
    if (!result) throw new AppError("Invalid email or password", 404);
    const results = compareSync(body.password, result.password);
    if (results) {
      result.password = undefined;
      let payload = { subject: result };
      let token = sign(payload, process.env.ACCESS_TOKEN_SECRET);
      return res.status(200).json({
        success: 1,
        message: "Login Success",
        token: token,
        data: {
          id: result.id,
          roleID: result.roleID,
          firstName: result.firstName,
          lastName: result.lastName,
          profileUrl: result.profileUrl,
        },
      });
    } else {
      throw new AppError("Invalid email or password", 404);
    }
  }),
};
