/**
 * @copyright2021 Intelliatech Solutions Pvt Ltd
 * @Developed By Shivam Asawa
 */
require("dotenv").config();
var errorHandler = require("./utils/errorController");
var createError = require("http-errors");

var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var bodyParser = require("body-parser");
var cors = require("cors");

var indexRouter = require("./routes/index");
var authRouter = require("./routes/authRoutes");
var employeeRouter = require("./routes/employeeRoutes");
var AppError = require("./utils/appError");
const upload = require("express-fileupload");
var app = express();

// for Generating Random String for Access Token
// require("crypto").randomBytes(64).toString("hex");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(cors());
app.use(bodyParser.json({ limit: "50mb" }));

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(upload());

app.use("/", indexRouter);
app.use("/Api/Auth", authRouter);
app.use("/Api/Employee", employeeRouter);

//For Error Handling
app.all("*", (req, res, next) => {
  throw new AppError(`Requested Url ${req.path} Not Found !`, 404);
});

// MiddleWare For Error Handling
app.use(errorHandler);


// For unit testing demo

const add = (a = 0, b = 0) => {
  if(typeof a !== 'number' || typeof b !== 'number') {
    return 0;
  }

  return a + b;
};

const sub = (a = 0, b = 0) => {
  if(typeof a !== 'number' || typeof b !== 'number') {
    return 0;
  }

  return a - b;
}

module.exports = {
  app,
  add,
  sub
};
