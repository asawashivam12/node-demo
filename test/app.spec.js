const assert = require('assert');
const { expect } = require ('chai')
const { add } = require ('../app')


describe('the add function', () => {
    it('should add 2 numbers together', () => {
        const result = add(2, 2);

        expect(result).to.be.eq(4)
    });

    it('should be able to handle 1 number', () => {
        const result = add(2);
        expect(result).to.be.eq(2);
    });
})