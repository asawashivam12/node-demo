const mysql = require("mysql");
const pool = mysql.createPool({
  connectionLimit: 10,
  host: process.env.HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.MYSQL_DB,
});

pool.getConnection((err, connection) => {
  if (err) throw err;
  if (connection) connection.release();
  console.log("Welcome IntelliaTech Solutions Your Database is Activated");
});

module.exports = pool;
