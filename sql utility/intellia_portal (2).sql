-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2021 at 03:16 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intellia_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `_id` int(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `contactNo` bigint(255) NOT NULL,
  `empRoleID` int(255) NOT NULL,
  `empReferenceID` varchar(255) NOT NULL,
  `dob` date DEFAULT NULL COMMENT 'YYYY-MM-DD',
  `city` varchar(255) NOT NULL,
  `address` longtext NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `empDesignation` varchar(255) NOT NULL,
  `iStatus` int(255) NOT NULL COMMENT 'either 1 or 0',
  `gender` int(255) NOT NULL COMMENT 'Male=1 Female=0 others=2',
  `profileUrl` varchar(255) NOT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`_id`, `firstName`, `lastName`, `email`, `password`, `contactNo`, `empRoleID`, `empReferenceID`, `dob`, `city`, `address`, `pincode`, `empDesignation`, `iStatus`, `gender`, `profileUrl`, `createdDate`, `updateDate`) VALUES
(1, 'Vikrant', 'Rana', 'vikrant01786@gmail.com', '$2b$10$8nO.WsjdFQLeXjfTINQVFOV60OUCqUBkmbNAuQiZPsCckxhv6bz2S', 7847838577, 1, '40', '1989-01-05', 'Bengaluru', '10B,Maa Sharda Nagar', '452001', 'Senior Software Engineer', 1, 0, 'http://localhost:9000/uploads/7d6cfebdfe5d964dc3598bfcc7478964.jpg', '2021-04-07 15:03:26', '2021-04-07 15:03:26'),
(2, 'shivam', 'Asawa', 'shivam@gmail.com', '$2b$10$GyyXOgUGMFMn15DxfjafpeLVOQPrB/TnzNVTwLmURi6FzmQGLLO82', 7867778787, 1, '40', '1989-01-05', 'Indore', '10B,Maa Sharda Nagar', '452001', 'Senior Software Engineer', 1, 0, '', '2021-04-07 17:46:30', '2021-04-07 17:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(255) NOT NULL,
  `roleName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roleName`) VALUES
(1, 'SuperAdmin'),
(2, 'Admin'),
(3, 'HR'),
(4, 'Employee');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
